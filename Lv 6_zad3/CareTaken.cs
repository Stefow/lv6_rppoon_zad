﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lv_6_zad3
{
    class CareTaker
    {
        private List<Memento> prethodnoStanje;
        private int ind;
        public CareTaker() 
        { 
            this.prethodnoStanje = new List<Memento>(); 
            ind = prethodnoStanje.Count - 1; 
        }
        public CareTaker(List<Memento> ps)
        {
            this.prethodnoStanje = ps;
            ind = ps.Count - 1;
        }
        public Memento Undo()
        {
            --ind;
            if (ind < 0)
            { 
                return null;
            }
            return prethodnoStanje[ind];
        }

        public void AddPreviousState(Memento stanje) 
        { 
            prethodnoStanje.Add(stanje);
            ind = prethodnoStanje.Count - 1; 
        }

        public Memento Redo()
        {
            ++ind;
            if (ind > prethodnoStanje.Count - 1)
            {
                return null;
            }
            return prethodnoStanje[ind];
        }

    }
}
