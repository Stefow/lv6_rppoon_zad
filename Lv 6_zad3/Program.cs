﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lv_6_zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker cT = new CareTaker();
            ToDoItem toDo = new ToDoItem("Labosi", "Riješi lavos ovog tjedna", new DateTime(2020, 5, 15));
            cT.AddPreviousState(toDo.StoreState());
            toDo.Rename("Novi Labosi");
            toDo.ChangeTask("Novi zadatak");
            toDo.ChangeTimeDue(new DateTime(2020, 5, 25));
            cT.AddPreviousState(toDo.StoreState());
            Console.WriteLine(toDo.ToString());
            Console.ReadLine();
            
        }
    }
}
