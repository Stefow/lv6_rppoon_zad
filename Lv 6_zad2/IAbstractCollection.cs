﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lv_6_zad2
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }

}
