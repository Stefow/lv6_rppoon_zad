﻿namespace Lv_6_zad2
{
    interface IAbstractIterator
    {
        Product First();
        Product Next();
        bool IsDone
        {
            get;
        }
        Product Current
        {
            get;
        }
    }
}