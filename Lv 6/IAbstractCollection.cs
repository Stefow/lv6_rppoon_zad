﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lv_6
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
