﻿using System;

namespace Lv_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("Ispit", "Sutra je ispit iz matematike"));
            notebook.AddNote(new Note("Trening", "20 sklekova u 15:20"));
            notebook.AddNote(new Note("Plivanje", "Od 18:00 do 20:00"));
            notebook.AddNote(new Note("Spisak za kupovinu", "Maslac, voda, sok, grickalice"));
            Iterator iterator = new Iterator(notebook);
            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}
